package com.example.flickr.data

import io.reactivex.Single
import retrofit2.http.GET

interface FlickrService {

    companion object {
        const val apiKey = "fee10de350d1f31d5fec0eaf330d2dba"
        const val host = "https://api.flickr.com/"
    }

    //https://api.flickr.com/services/rest/?method=flickr.photos.getRecent&api_key=fee10de350d1f31d5fec0eaf330d2dba&format=json&nojsoncallback=true
    @GET("services/rest/?method=flickr.photos.getRecent&api_key=${apiKey}&per_page=20&format=json&nojsoncallback=true")
    fun getRecentPhotos() : Single<RecentPhotoResponse>
}