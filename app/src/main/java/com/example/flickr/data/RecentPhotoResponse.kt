package com.example.flickr.data

data class RecentPhotoResponse(
    val photos: PhotosField,
    val stat: String
)

data class PhotosField(
    val page: Int,
    val photo: List<FlickrImage>
)