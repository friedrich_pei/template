package com.example.flickr.data

data class FlickrImage(
    val id: String,
    val server: String,
    val secret: String,
    val farm: Int,
    val title: String
) {
    fun thumbnailUrl() = "https://farm$farm.staticflickr.com/$server/${id}_${secret}_t.jpg"

    fun largeUrl() = "https://farm$farm.staticflickr.com/$server/${id}_${secret}_b.jpg"
}