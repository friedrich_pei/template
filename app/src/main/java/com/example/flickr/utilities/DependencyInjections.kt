package com.example.flickr.utilities

import com.example.flickr.data.FlickrService
import com.example.flickr.repository.RecentPhotosRepository
import com.example.flickr.repository.RecentPhotosRepositoryImpl
import com.example.flickr.rx.NetworkScheduler
import com.example.flickr.viewmodel.HomeViewModelFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object DependencyInjections {
    fun provideFlickrService(): FlickrService {

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

        val retrofit = Retrofit.Builder()
            .baseUrl(FlickrService.host)
            .client(client)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        return retrofit.create(FlickrService::class.java)
    }

    fun provideRxScheduler(): NetworkScheduler {
        return NetworkScheduler()
    }

    fun provideRecentPhotosRepository(): RecentPhotosRepository {
        return RecentPhotosRepositoryImpl(provideFlickrService(), provideRxScheduler())
    }

    fun provideHomeViewModelFactory(): HomeViewModelFactory {
        return HomeViewModelFactory(provideRecentPhotosRepository())
    }
}