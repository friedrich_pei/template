package com.example.flickr

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.flickr.databinding.FragmentHomeBinding
import com.example.flickr.ui.ImageAdapter
import com.example.flickr.utilities.DependencyInjections
import com.example.flickr.viewmodel.HomeViewModel
import com.example.flickr.viewmodel.SharedViewModel

class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding
    private lateinit var sharedViewModel: SharedViewModel

    private val viewModel: HomeViewModel by viewModels {
        DependencyInjections.provideHomeViewModelFactory()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedViewModel = activity?.run {
            ViewModelProviders.of(this)[SharedViewModel::class.java]
        } ?: throw IllegalStateException("Invalid Activity")

        binding.sharedVM = sharedViewModel

        binding.photoList.layoutManager = LinearLayoutManager(context)

        val adapter = ImageAdapter()
        binding.photoList.adapter = adapter

        viewModel.photos.observe(viewLifecycleOwner, Observer { newPhotos -> adapter.submitList(newPhotos) })

        viewLifecycleOwner.lifecycle.addObserver(viewModel)

        // quick return animation
        val titleView = binding.textHomeTitle
        binding.photoList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy == 0) {
                    return
                }
                val newVisibility = if (dy < 0) View.VISIBLE else View.GONE
                titleView.visibility = newVisibility
            }
        })
    }
}