package com.example.flickr.repository

import com.example.flickr.data.FlickrImage
import com.example.flickr.data.FlickrService
import com.example.flickr.rx.RxScheduler
import com.example.flickr.ui.DisplayImage
import io.reactivex.Single

interface RecentPhotosRepository {
    fun getRecentPhotos(): Single<List<DisplayImage>>
}

class RecentPhotosRepositoryImpl(
    private val service: FlickrService,
    private val scheduler: RxScheduler
) : RecentPhotosRepository {
    override fun getRecentPhotos(): Single<List<DisplayImage>> {
        return service.getRecentPhotos()
            .subscribeOn(scheduler.io())
            .map { res -> res.photos.photo.map { image -> toDisplayPhoto(image) } }
            .observeOn(scheduler.ui())
    }
}

fun toDisplayPhoto(image: FlickrImage): DisplayImage {
    return DisplayImage(id = image.id, thumbnail = image.thumbnailUrl(), largeImg = image.largeUrl(), title = image.title)
}