package com.example.flickr.databinding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

@BindingAdapter("imageUrl")
fun loadRemoteImage(view: ImageView, url: String) {
    Picasso.get().load(url).into(view)
}