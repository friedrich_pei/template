package com.example.flickr.ui

class DisplayImage(val id: String, val thumbnail: String, val largeImg: String, val title: String)