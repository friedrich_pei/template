package com.example.flickr.ui

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.flickr.HomeFragmentDirections
import com.example.flickr.databinding.ViewImageItemBinding

class ImageAdapter : ListAdapter<DisplayImage, ImageViewHolder>(ImageDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        return ImageViewHolder(
            ViewImageItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

}

class ImageDiffCallback : DiffUtil.ItemCallback<DisplayImage>() {
    override fun areItemsTheSame(oldItem: DisplayImage, newItem: DisplayImage): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: DisplayImage, newItem: DisplayImage): Boolean {
        return oldItem.id == newItem.id
    }
}

class ImageViewHolder(private val binding: ViewImageItemBinding) :
    RecyclerView.ViewHolder(binding.root) {
    init {
        binding.setItemClickListener {
            binding.displayImage?.let { data ->
                val action = HomeFragmentDirections.actionHomeFragmentToDetailFragment(
                    imageUrl = data.largeImg,
                    title = data.title
                )
                it.findNavController().navigate(action)
            }
        }
    }

    fun bind(item: DisplayImage) {
        binding.displayImage = item
        binding.executePendingBindings()
    }
}