package com.example.flickr.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SharedViewModel : ViewModel() {
    val displayTitle = MutableLiveData<String>()

    init {
        displayTitle.value = "Title"
    }
}