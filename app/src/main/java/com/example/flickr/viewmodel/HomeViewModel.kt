package com.example.flickr.viewmodel

import androidx.lifecycle.*
import com.example.flickr.repository.RecentPhotosRepository
import com.example.flickr.ui.DisplayImage
import io.reactivex.disposables.Disposable

class HomeViewModel(private val repo: RecentPhotosRepository) : ViewModel(), LifecycleObserver {
    val photos: MutableLiveData<List<DisplayImage>> = MutableLiveData()
    var disposable: Disposable? = null

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun start() {
        disposable?.dispose()

        disposable = repo.getRecentPhotos()
            .subscribe({ result -> photos.value = result }, { })
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun stop() {
        disposable?.dispose()
    }
}