package com.example.flickr.rx

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

interface RxScheduler {
    fun io() : Scheduler
    fun ui() : Scheduler
}

class NetworkScheduler : RxScheduler {
    override fun io(): Scheduler {
        return Schedulers.io()
    }

    override fun ui(): Scheduler {
        return AndroidSchedulers.mainThread()
    }
}