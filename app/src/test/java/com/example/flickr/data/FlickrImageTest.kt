package com.example.flickr.data

import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class FlickrImageTest {

    @Test
    fun testUrl() {
        val image = FlickrImage(id = "1", server = "server1", secret = "secret2", farm = 100, title = "hello")
        Assert.assertEquals("https://farm100.staticflickr.com/server1/1_secret2_t.jpg", image.thumbnailUrl())
        Assert.assertEquals("https://farm100.staticflickr.com/server1/1_secret2_b.jpg", image.largeUrl())
    }
}