package com.example.flickr.repository

import com.example.flickr.data.FlickrImage
import com.example.flickr.data.FlickrService
import com.example.flickr.data.PhotosField
import com.example.flickr.data.RecentPhotoResponse
import com.example.flickr.rx.RxScheduler
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class RecentPhotosRepositoryTest {
    private val scheduler = object : RxScheduler {
        override fun io(): Scheduler {
            return Schedulers.trampoline()
        }

        override fun ui(): Scheduler {
            return Schedulers.trampoline()
        }
    }

    val img1 = FlickrImage(
        id = "1",
        server = "server",
        secret = "secret",
        farm = 2,
        title = "hello"
    )

    val img2 = FlickrImage(
        id = "333",
        server = "server",
        secret = "secret",
        farm = 2,
        title = "hello"
    )

    private val service = object : FlickrService {
        override fun getRecentPhotos(): Single<RecentPhotoResponse> {
            return Single.just(
                RecentPhotoResponse(
                    stat = "ok", photos = PhotosField(
                        page = 0, photo = listOf(img1, img2)
                    )
                )
            )
        }
    }

    private val impl = RecentPhotosRepositoryImpl(service, scheduler)

    @Test
    fun getRecentPhotos() {
        impl.getRecentPhotos()
            .test()
            .assertValueCount(1)
            .assertValue { value -> value.size == 2 && value[0].id == "1" }
            .assertNoErrors()
            .assertComplete()
            .dispose()
    }
}