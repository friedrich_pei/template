package com.example.flickr.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.flickr.repository.RecentPhotosRepository
import com.example.flickr.ui.DisplayImage
import io.reactivex.Single
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class HomeViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private val repo = object : RecentPhotosRepository {
        override fun getRecentPhotos(): Single<List<DisplayImage>> {
            return Single.just(
                listOf(
                    DisplayImage(
                        id = "123",
                        thumbnail = "1.jpg",
                        largeImg = "2.jpg0",
                        title = "Hello"
                    )
                )
            )
        }
    }

    private lateinit var vm: HomeViewModel

    @Before
    fun setUp() {
        vm = HomeViewModel(repo)
    }

    @Test
    fun start() {
        Assert.assertNull(vm.disposable)
        Assert.assertNull(vm.photos.value)
        vm.start()
        val result = vm.photos.value
        Assert.assertNotNull(result)
        Assert.assertEquals(1, result!!.size)
        Assert.assertNotNull(vm.disposable)
    }

    @Test
    fun stop() {
        Assert.assertNull(vm.disposable)
        vm.disposable = Single.never<Int>().subscribe()
        Assert.assertFalse(vm.disposable!!.isDisposed)
        vm.stop()
        Assert.assertTrue(vm.disposable!!.isDisposed)
    }

}